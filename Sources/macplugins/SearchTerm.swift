//
//  File.swift
//  
//
//  Created by John Endres on 12/27/21.
//

import Foundation

/// Naive converter for URLs
///
/// For converting strings to URL compatible search terms.  Replaces spaces in the string with +.  This
/// example shows how to search iTumes for "Pink Floyd":
///
///     @SearchTerm var term = "Pink Floyd"
///
///     guard let url = URL(string: "https://itunes.apple.com/search?term=\(term)&entity=album") else {
///         return []
///     }
@propertyWrapper public struct SearchTerm {
    /// Transform + to space
    ///
    /// - Parameter string: string to convert
    /// 
    /// - Returns: converted string
    private static func transform(_ string: String) -> String {
        string.replacingOccurrences(of: " ", with: "+")
    }

    public var wrappedValue: String {
        didSet {
            wrappedValue = Self.transform(wrappedValue)
        }
    }
    
    public init(wrappedValue: String) {
        self.wrappedValue = Self.transform(wrappedValue)
    }
}
