//
//  Capitalized.swift
//  
//
//  Created by John Endres on 12/27/21.
//

import Foundation

/// Simple and maybe useless property wrapper
///
/// Converts the value assigned to the property into a string with all words capitalized.
/// My guess is that this was done as a simple example for a CocoaHeads talk?
@propertyWrapper public struct Capitalized {
    /// Capitalize the string
    ///
    /// - Parameter string: String to capitalize
    ///
    /// - Returns: Capitalized version of string
    private static func transform(_ string: String) -> String {
        string.capitalized
    }

    public var wrappedValue: String {
        didSet {
            wrappedValue = Self.transform(wrappedValue)
        }
    }
    
    public init(wrappedValue: String) {
        self.wrappedValue = Self.transform(wrappedValue)
    }
}
