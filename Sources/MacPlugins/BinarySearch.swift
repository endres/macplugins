//
//  BinarySearch.swift
//  
//
//  Created by John Endres on 5/29/24.
//

import Foundation
import Algorithms

extension RandomAccessCollection where Element: Comparable {
    /// Search the given sorted collection
    ///
    /// This does a binary search on a sorted collection using `partitioningIndex`
    /// from the Algorithms package.
    ///
    /// - Parameter element: element to search for
    /// - Returns: Index of element if found, nil if not
    /// - Important: Binary search requires an already sorted array
    public func binarySearch(_ element: Element) -> Index? {
        let index = partitioningIndex { collectionElement in
            collectionElement >= element
        }
        let found = index != endIndex && self[index] == element

        return found ? index : nil
    }
}
