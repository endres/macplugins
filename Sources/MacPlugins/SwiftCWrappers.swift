//
//  SwiftCWrappers.swift
//
//
//  Created by John Endres on 5/31/24.
//

import Foundation

/// Convert error number to Swift string
///
/// This is a Swift based wrapper around C's `strerror()` function
/// 
/// - Parameter error: error to convert.  Defaults to C's `errno`
/// - Returns: function accepts an error number argument errnum and returns a pointer to
///            the corresponding message string
public func stringerror(_ error: Int32 = errno) -> String {
    return String(cString: strerror(error))
}
