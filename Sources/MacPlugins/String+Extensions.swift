//
//  String+Extensions.swift
//
//
//  Created by John Endres on 6/14/24.
//

import Foundation

extension String {
    /// Reformat a string into the format given by the provided mask..
    ///
    /// The mask uses X as a placeholder for each character it finds in the string
    /// and this will replace each with the next character in the sequence of the string
    /// until it runs out.
    ///
    /// Example:
    ///
    ///     "11015551212".reformat(to: "+X (XXX) XXX-XXXX") gives "+1 (101) 555-1212"
    ///     "5551212".reformat(to: "XXX-XXXX") gives "555-1212"
    ///     "1015551212".reformat(to: "XXX-XXX-XXXX") gives "101-555-1212"
    ///     "11015551212".reformat(to: "X-XXX-XXX-XXXX") gives "1-101-555-1212"
    ///     "1ABC5551212".reformat(to: "+X (XXX) XXX-XXXX", numbersOnly: false) gives "+1 (ABC) 555-1212"
    ///
    /// - Parameters:
    ///   - mask: Format of returned string with X's as a replacement from the source string
    ///   - numbersOnly: true if only numbers are used in the reformat, false to use the entire string
    /// - Returns: String reformatted into as much of the mask as it can use
    /// - SeeAlso: [Formatting Phone number in Swift](https://stackoverflow.com/questions/32364055/formatting-phone-number-in-swift)
    public func reformat(to mask: String, numbersOnly: Bool = true) -> String {
        let source = switch numbersOnly {
        case true:
            replacingOccurrences(of: "[^0-9]", with: "", options: .regularExpression)
        case false:
            self
        }
        var result = ""
        var index = source.startIndex // source iterator
 
        // iterate over the mask characters until the iterator of source ends
        for ch in mask where index < source.endIndex {
            if ch == "X" {
                // take the next letter from the source string
                result.append(source[index])
 
                // move source iterator to the next index
                index = source.index(after: index)
            } else {
                result.append(ch) // just append a mask character
            }
        }
        
        return result
    }
}
