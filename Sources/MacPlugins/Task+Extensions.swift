//
//  Task+Extensions.swift
//  RetryingTask
//
//  Created by Endres, John on 2/23/24.
//

import Foundation

/// State of the polling process
///
/// Used by the retry function to communicate the state of the retries.  If
/// the process feels like it shoudl try again, it should return .retry, if it
/// has been successful it should return .success(the result), and if there
/// is an unrecoverable error, it should return .failure(error to throw).
public enum PollingState<Success, Failure> where Failure : Error {
    /// Polling is incomplete, try again
    case retry
    
    /// A success, storing a `Success` value.
    case success(Success)

    /// A failure, storing a `Failure` value.
    case failure(Failure)
}

/// Retry Failed
///
/// The number of retries has been reached without a success or non-recoverable
/// error.  The Task with throw this to indicate that.
public struct RetryFailed: Error {
    /// Holds the number of retries attempted
    public let attempts: Int
}

/// Fine tuned interval values
///
/// Conversion constants for the time interval.  Since Task's sleep function measures
/// in nanoseconds, these values represent the number of nanosecods for the
/// indicated value
public enum TaskRetryUnits: TimeInterval {
    // Number of nanseconds in one nanosecond (one billionth of a second.)
    // Excluded here because the Task API uses nanoseconds, but the sleep
    // seems limited to microseconds.
    // case nanoseconds = 1

    /// Number of nanseconds in one microsecond (one millionth of a second.)
    case microseconds = 1_000

    /// Number of nanseconds in one milisecond.  (one thousandth of a second)
    case milliseconds = 1_000_000

    /// Number of nanseconds in one second
    case seconds = 1_000_000_000
}

public extension Task where Failure == Error {
    /// Create a retrying Task
    ///
    /// Creates a Task that will continue its operation until it succeeds with a value,
    /// it errors out so many times, or signals complete with one of the special error types.
    /// The only way to stop the polling/retries is by returning a value.  Any thrown error will signal
    /// the retry mechanism.
    /// - Parameters:
    ///   - priority: Task's priority
    ///   - maxRetryCount: Maximum attempts to make.
    ///   - retryDelay: The delay between tries based on the retryUnits parameter.  Default is delay this many seconds
    ///   - retryUnits: The time unit for delay.  Default is 1_000_000_000 which is one second
    ///   - operation: The body of the task.  The parameter here is what retry we are on starting at 0, and result is a PollingState value.
    /// - Returns: The created task that loops using the retry information
    @discardableResult
    static func retrying(
        priority: TaskPriority? = nil,
        maxRetryCount: Int,
        retryDelay: TimeInterval,
        retryUnits: TaskRetryUnits = .seconds,
        operation: @Sendable @escaping (Int) async -> PollingState<Success, Failure>
    ) -> Task {
        Task(priority: priority) {
            for number in 0..<maxRetryCount {
                switch await operation(number) {
                case .retry:
                    if retryDelay > 0 {
                        // Note: this will throw CancellationError if the task was cancelled
                        try await Task<Never, Never>.sleep(nanoseconds: UInt64(retryUnits.rawValue * retryDelay))
                    }
                case .success(let value):
                    return value
                case .failure(let error):
                    throw error
                }
            }
            
            throw RetryFailed(attempts: maxRetryCount)
        }
    }
}
