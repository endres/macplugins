//
//  PowerSet.swift
//  Macplugins
//
//  Created by John Endres on 8/26/24.
//

import Foundation
import Algorithms

extension Array {
    /// Return a power set of the current array as a sequence for efficiency (to avoid
    /// array copies while working with this if this was a collection)
    ///
    /// Power set P(S) of a set S is the set of all subsets of S. For example S = {a, b, c} then P(s) = {{}, {a}, {b}, {c}, {a,b}, {a, c}, {b, c}, {a, b, c}}.
    ///
    /// - SeeAlso: [PowerSet](http://www.geeksforgeeks.org/power-set/)
    public var powerset: CombinationsSequence<[Element]> {
        return combinations(ofCount: 0...count)
    }
}
