//
//  CapitalizedTests.swift
//  
//
//  Created by John Endres on 12/27/21.
//

import XCTest
@testable import Macplugins

class SearchTermTests: XCTestCase {

    @SearchTerm var value = ""
    
    override func setUpWithError() throws {
        value = ""
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testConversion() throws {
        value = "lower cased word"
        
        XCTAssertEqual(value, "lower+cased+word")
        print(value)
    }

}
