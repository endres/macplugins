//
//  Task+ExtensionsTests.swift
//  RetryingTaskTests
//
//  Created by Endres, John on 2/23/24.
//

import XCTest
@testable import Macplugins

/// For the use cases to show what iteration the unrecoverable error happened in.
private enum SampleError: Error {
    /// Failed with the number of the iteration it failed on
    case failed(Int)
}

/// Collects an array of time intervals between calls to the `record` function..
/// An actor is like a class that is safe to use in concurrent environments without the
/// need for locks or semaphores.
private actor IntervalCollector {
    /// Last call date
    private var last: Date?
    
    /// The intervals between calls
    private(set) var times = [TimeInterval]()
    
    func record(date: Date = Date()) {
        // If we have a previous call, add to the times array
        // the difference between now and that call.
        if let last {
            times.append(date.timeIntervalSince(last))
        }
        
        // Remember this call for next time
        last = date
    }
}

private extension Double {
    /// Test equality of 2 doubles within a certain amount of precision
    /// - Parameters:
    ///   - value: Value to test
    ///   - precise: How precise we are testing for
    /// - Returns: True if equal to that amount of precision.
    func equal(_ value: Double, precise: Int) -> Bool {
        let denominator: Double = pow(10.0, Double(precise))
        let maxDiff: Double = 1 / denominator
        let realDiff: Double = self - value

        if fabs(realDiff) <= maxDiff {
            return true
        } else {
            return false
        }
    }
}

final class Task_ExtensionsTests: XCTestCase {

    let iterationBreakCount = 8
    
    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

    func testRetryToError() async throws {
        do {
            let result = try await Task<String, Error>.retrying(maxRetryCount: 10, retryDelay: 1, retryUnits: .microseconds) { number in
                if number < self.iterationBreakCount {
                    return .retry
                }
                
                return .failure(SampleError.failed(number))
            }
                .value
            
            XCTFail("Failure value: \(result)")
        } catch SampleError.failed(let count) {
            XCTAssertEqual(count, iterationBreakCount)
        } catch {
            XCTFail("Wrong type")
        }
    }

    func testRetryToSuccess() async throws {
        do {
            let result = try await Task<Int, Error>.retrying(maxRetryCount: 10, retryDelay: 1, retryUnits: .microseconds) { number in
                if number < self.iterationBreakCount {
                    return .retry
                }
                
                return .success(number)
            }
                .value
            
            XCTAssertEqual(result, iterationBreakCount)
        } catch {
            XCTFail("Wrong type")
        }
    }

    func testRetriesFailed() async throws {
        do {
            _ = try await Task<Int, Error>.retrying(maxRetryCount: 10, retryDelay: 1, retryUnits: .microseconds) { number in
                return .retry
            }
                .value
            
            XCTFail("Should have thrown an error")
        } catch let error as RetryFailed {
            XCTAssertEqual(error.attempts, 10)
        } catch {
            XCTFail("Wrong type")
        }
    }
    
    func testRetryTimingInSeconds() async throws {
        let intervals = IntervalCollector()
        
        _ = await Task<Int, Error>.retrying(maxRetryCount: 10, retryDelay: 1, retryUnits: .seconds) { number in
            await intervals.record()
            
            return .retry
        }
            .result

        let count = await intervals.times.count
        let sum = await intervals.times.reduce(0, +)
        let average = sum / Double(count)

        XCTAssertEqual(count, 9)
        XCTAssertTrue(average.equal(1, precise: 1), "average: \(average)")
    }
    
    func testRetryTimingInMilliseconds() async throws {
        let intervals = IntervalCollector()
        
        _ = await Task<Int, Error>.retrying(maxRetryCount: 10, retryDelay: 1, retryUnits: .milliseconds) { number in
            await intervals.record()
            
            return .retry
        }
            .result

        let count = await intervals.times.count
        let sum = await intervals.times.reduce(0, +)
        let average = sum / Double(count)

        XCTAssertEqual(count, 9)
        XCTAssertTrue(average.equal(0.001, precise: 1), "average: \(average)")
    }
    
    func testRetryTimingInMicroseconds() async throws {
        let intervals = IntervalCollector()
        
        _ = await Task<Int, Error>.retrying(maxRetryCount: 10, retryDelay: 1, retryUnits: .microseconds) { number in
            await intervals.record()
            
            return .retry
        }
            .result

        let count = await intervals.times.count
        let sum = await intervals.times.reduce(0, +)
        let average = sum / Double(count)

        XCTAssertEqual(count, 9)
        XCTAssertTrue(average.equal(0.000001, precise: 1), "average: \(average)")
    }

}
