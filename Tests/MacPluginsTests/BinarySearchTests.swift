//
//  BinarySearchTests.swift
//  
//
//  Created by John Endres on 5/29/24.
//

import XCTest
@testable import Macplugins

final class BinarySearchTests: XCTestCase {

    let subject = Array(stride(from: 1, to: 1_000_000, by: 2))
    
    override func setUpWithError() throws {
    }

    override func tearDownWithError() throws {
    }

    func testExists() throws {
        XCTAssertNotNil(subject.binarySearch(125))
    }

    func testMissing() throws {
        XCTAssertNil(subject.binarySearch(756))
    }
    
    func testPerformanceExists() throws {
        self.measure {
            XCTAssertNotNil(subject.binarySearch(125))
        }
    }
    
    func testPerformanceMissing() throws {
        self.measure {
            XCTAssertNil(subject.binarySearch(756))
        }
    }

}
