//
//  String+ExtensionsTests.swift
//
//
//  Created by John Endres on 6/14/24.
//

import Testing

struct StringExtensionsTests {

    @Test func testFormats() async throws {
        // Write your test here and use APIs like `#expect(...)` to check expected conditions.
        #expect("11015551212".reformat(to: "+X (XXX) XXX-XXXX") == "+1 (101) 555-1212")
        #expect("1ABC5551212".reformat(to: "+X (XXX) XXX-XXXX", numbersOnly: false) == "+1 (ABC) 555-1212")
        #expect("5551212".reformat(to: "XXX-XXXX") == "555-1212")
        #expect("1015551212".reformat(to: "XXX-XXX-XXXX") == "101-555-1212")
        #expect("11015551212".reformat(to: "X-XXX-XXX-XXXX") == "1-101-555-1212")
    }

}
