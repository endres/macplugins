//
//  SwiftCWrappersTests.swift
//  
//
//  Created by John Endres on 5/31/24.
//

import XCTest
@testable import Macplugins

final class SwiftCWrappersTests: XCTestCase {

    func testExample() throws {
        XCTAssertEqual(stringerror(0), "Undefined error: 0")
        XCTAssertEqual(stringerror(EPERM), "Operation not permitted")
        XCTAssertEqual(stringerror(ENOENT), "No such file or directory")
        XCTAssertEqual(stringerror(ESRCH), "No such process")
        XCTAssertEqual(stringerror(EINTR), "Interrupted system call")
        XCTAssertEqual(stringerror(EIO), "Input/output error")
    }

}
