//
//  CapitalizedTests.swift
//  
//
//  Created by John Endres on 12/27/21.
//

import XCTest
@testable import Macplugins

class CapitalizedTests: XCTestCase {

    @Capitalized var value = ""
    
    override func setUpWithError() throws {
        value = ""
    }

    override func tearDownWithError() throws {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    func testCapitalization() throws {
        value = "lower cased word"
        
        XCTAssertEqual(value, "Lower Cased Word")
        print(value)
    }

}
