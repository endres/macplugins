//
//  ArrayPowerSetTests.swift
//  Macplugins
//
//  Created by John Endres on 8/26/24.
//

import Testing
@testable import Macplugins

struct ArrayPowerSetTests {

    @Test func testEmptyArray() {
        let subject = [Int]()
        let powerSet = subject.powerset

        #expect(Array(powerSet) == [[]])
    }

    @Test func testOneValueArray() {
        let subject = [1]
        let powerSet = subject.powerset

        #expect(Array(powerSet) == [[], [1]])
    }

    @Test func testDualValueArray() {
        let subject = [1, 1]
        let powerSet = subject.powerset

        #expect(Array(powerSet) == [[], [1], [1], [1, 1]])
    }

    @Test func testLargeArray() {
        let subject = [1, 2, 3, 4, 5]
        let powerSet = subject.powerset
        let expected = [
            [5, 4, 3, 2, 1],
            [4, 3, 2, 1],
            [5, 3, 2, 1],
            [3, 2, 1],
            [5, 4, 2, 1],
            [4, 2, 1],
            [5, 2, 1],
            [2, 1],
            [5, 4, 3, 1],
            [4, 3, 1],
            [5, 3, 1],
            [3, 1],
            [5, 4, 1],
            [4, 1],
            [5, 1],
            [1],
            [5, 4, 3, 2],
            [4, 3, 2],
            [5, 3, 2],
            [3, 2],
            [5, 4, 2],
            [4, 2],
            [5, 2],
            [2],
            [5, 4, 3],
            [4, 3],
            [5, 3],
            [3],
            [5, 4],
            [4],
            [5],
            []
        ]

        #expect(powerSet.count == 32)
        #expect(powerSet.count == expected.count)

        for value in expected {
            #expect(powerSet.contains(value.sorted()))
        }
    }

}
