# macplugins

## Description

Gradually growing list of useful functions.

## License

[MIT](https://choosealicense.com/licenses/mit/)

## Summary

1. **BinarySearch** -- Binary search using SwiftAlgorithms
2. **Capitalized** -- Property wrapper that will give a capitalized string
3. **SearchTerm** -- Overly simplistic property wrapper for converting strings to URL compatible search terms

## Adding Macplugins as a dependency

To use the `Macplugins ` library in a SwiftPM project, 
add the following line to the dependencies in your `Package.swift` file:

```swift
.package(url: "https://bitbucket.org/endres/macplugins", from: "1.0.0"),
```

Include `"Macplugins"` as a dependency for your executable target:

```swift
.target(name: "<target>", dependencies: [
    .product(name: "Macplugins", package: "macplugins"),
]),
```

Finally, add `import Macplugins` to your source code.
